<footer class="footer" role="contentinfo">
    <div id="inner-footer" class="row">
        <div class="footer__menu small-12 large-3 columns">
            <nav role="navigation">
                <!--                            --><?php //joints_footer_links(); ?>
                <span class="title">menu</span>
                <ul class="no-bullet">
                    <li><a href="#">opinie</a></li>
                    <li><a href="#">o nas</a></li>
                    <li><a href="#">oferta</a></li>
                    <li><a href="#">kategorie</a></li>
                    <li><a href="#">nasi klienci</a></li>
                    <li><a href="#">impresariat</a></li>
                    <li><a href="#">kontakt</a></li>
                </ul>
            </nav>
        </div>
        <div class="footer__categories small-12 large-3 columns">
            <span class="title">kategorie</span>
            <ul class="no-bullet">
                <li><a href="#">muzyka</a></li>
                <li><a href="#">programy artystyczne</a></li>
                <li><a href="#">organizacja imprez</a></li>
                <li><a href="#">imprezy firmowe</a></li>
                <li><a href="#">technika sceniczna</a></li>
                <li><a href="#">lokalizacje</a></li>
            </ul>
        </div>
        <div class="footer__offer small-12 large-3 columns">
            <span class="title">oferta</span>
            <ul class="no-bullet">
                <li><a href="#">katalog</a></li>
                <li><a href="#">newsletter</a></li>
                <li><a href="#">impresariat</a></li>
            </ul>
        </div>

        <div class="footer__social small-12 large-3 columns">
            <span class="title">social media</span>
                          <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4810.7 8046 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M89.8,14.3a8.247,8.247,0,0,1-2.6,1,3.991,3.991,0,0,0-3-1.3,4.034,4.034,0,0,0-4.1,4,2.769,2.769,0,0,0,.1.9,12.234,12.234,0,0,1-8.5-4.2,3.606,3.606,0,0,0-.6,2,4.172,4.172,0,0,0,1.8,3.4,4.136,4.136,0,0,1-1.9-.5v.1a4.074,4.074,0,0,0,3.3,4,3.75,3.75,0,0,1-1.1.1,2.2,2.2,0,0,1-.8-.1,3.992,3.992,0,0,0,3.8,2.8,8.24,8.24,0,0,1-5.1,1.7,3.4,3.4,0,0,1-1-.1,12.194,12.194,0,0,0,6.3,1.8A11.582,11.582,0,0,0,88.1,18.4v-.5a7.957,7.957,0,0,0,2-2.1,8.44,8.44,0,0,1-2.4.6,3.362,3.362,0,0,0,2.1-2.1M79.3,0a22,22,0,1,0,22,22,22,22,0,0,0-22-22m0,42a20,20,0,1,1,20-20,20.059,20.059,0,0,1-20,20"
                                      transform="translate(-4868 8046)"/>
                            </svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4745 8046 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M22,0A22,22,0,1,0,44,22,22,22,0,0,0,22,0m0,42A20,20,0,1,1,42,22,20.059,20.059,0,0,1,22,42m2-24.8c0-.8.1-1.2,1.3-1.2H27V13H24.3c-3.2,0-4.3,1.5-4.3,4v2H18v3h2v9h4V22h2.7l.4-3h-3Z"
                                      transform="translate(-4745 8046)"/>
                            </svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4679 8045.9 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M43.1,94.8h.4a1.666,1.666,0,0,0,1.7-1.6V90.5a1.605,1.605,0,0,0-1.7-1.6h-.4a1.666,1.666,0,0,0-1.7,1.6v2.7a1.735,1.735,0,0,0,1.7,1.6m-.4-4.4a.6.6,0,0,1,1.2,0v2.9a.6.6,0,0,1-1.2,0Zm4.6,4.4a2.93,2.93,0,0,0,1.6-.7v.6H50V88.8H48.8v4.5s-.4.5-.8.5-.4-.3-.4-.3V88.8H46.4V94c-.1,0,0,.8.9.8m-9-.1h1.4V91.5L41.4,87H40l-1,3-1-3H36.6l1.8,4.5v3.2Zm8.1,6.8a.855.855,0,0,0-.6.3v4.1a.855.855,0,0,0,.6.3c.6,0,.6-.7.6-.7v-3.3s-.1-.7-.6-.7M44,76.9a22,22,0,1,0,22,22,22,22,0,0,0-22-22m0,42a20,20,0,1,1,20-20,20.059,20.059,0,0,1-20,20m7.6-22.8s-3.8-.2-7.6-.2-7.6.2-7.6.2a3.031,3.031,0,0,0-3.1,3,24.217,24.217,0,0,0,0,7.6,3.1,3.1,0,0,0,3.1,3s3.7.2,7.6.2c3.7,0,7.6-.2,7.6-.2a3.031,3.031,0,0,0,3.1-3,24.217,24.217,0,0,0,0-7.6,3.1,3.1,0,0,0-3.1-3M39.5,99.5H38v7.8H36.6V99.5H35V98.2h4.5Zm3.9,7.8H42.2v-.6a2.487,2.487,0,0,1-1.5.7.911.911,0,0,1-.9-.8v-6.1H41v5.7s0,.3.4.3.8-.5.8-.5v-5.5h1.2Zm4.9-1.5s0,1.6-1.1,1.6a1.515,1.515,0,0,1-1.3-.7v.6H44.6V98.2h1.3v2.9a2.024,2.024,0,0,1,1.3-.7c.8,0,1.1.7,1.1,1.6Zm4.8-3.7v2H50.5v1.5s0,.7.6.7.6-.7.6-.7v-.7H53V106a1.759,1.759,0,0,1-1.9,1.4,1.92,1.92,0,0,1-2-1.4v-3.8a1.774,1.774,0,0,1,2-1.7c2.1,0,2,1.6,2,1.6m-1.9-.6c-.6,0-.6.7-.6.7v.9h1.2v-.9s0-.7-.6-.7"
                                      transform="translate(-4701 7969)"/>
                            </svg></span>
        </div>
    </div> <!-- end #inner-footer -->
    <div class="copyright">
        <p>Design by <a href="www.beectrl.com">BeeCtrl</a></p>
    </div>
</footer> <!-- end .footer -->
</div>  <!-- end .main-content -->
</div> <!-- end .off-canvas-wrapper-inner -->
</div> <!-- end .off-canvas-wrapper -->
<?php wp_footer(); ?>

</body>
</html> <!-- end page -->