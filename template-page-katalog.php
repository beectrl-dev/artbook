<?php
/*
Template Name: Template Katalog
*/
?>

<?php get_header(); ?>

    <div id="content" class="clearfix">

        <div id="inner-content" class="clearfix">

            <main id="main" class="large-12 medium-12 columns np" role="main">

                <article>

                    <div class="photo">
                        <div class="photo__back">
                            <a class="photo__back--prev" href="#">Wróć</a>
                        </div>
                    </div>

                    <header class="page-subtitle">
                    </header> <!-- end article header -->

                    <section class="entry-content" itemprop="articleBody">
                        <div class="page-profiles row">

                            <div class="page-profiles__sidebar small-12 large-3 columns">
                                <ul class="page-profiles__sidebar__accordion accordion" data-accordion
                                    data-allow-all-closed="true">
                                    <?
                                    $catalog_list = array('muzyka', 'programy_artystyczne', 'organizacja_imprez', 'technika_scenicza', 'lokalizacje');
                                    foreach ($catalog_list as $index => $catalog) {
                                    $obj = get_post_type_object($catalog);
                                    ?>
                                    <li class="page-profiles__sidebar__accordion__item accordion-item <? echo ($index == 0)? 'is-active': '';?>"
                                        data-accordion-item>
                                        <a href="#" class="accordion-title"> <? echo $obj->labels->singular_name ?></a>
                                        <?
                                        echo '<div class="accordion-content" data-tab-content><div class="row">';
                                        $args = array(
                                            'post_type' => $catalog,
                                            'orderby' => 'title',
                                            'post_parent' => 0,
                                            'posts_per_page' => '999'
                                        );

                                        $subcategories = get_posts($args);
                                        foreach ($subcategories as $index => $subcategory) { ?>

                                            <div class="small-12 columns"><a
                                                    href="#"><? echo $subcategory->post_title; ?></a>
                                            </div>
                                            <?
                                        }
                                        echo '</div></div></li>';
                                        }

                                        ?>

                                    <!-- ... -->
                                </ul>
                            </div>


                            <div class="page-profiles__container small-12 large-9 columns">

                                <div id="profile" class="page-profiles__container__accordion" data-accordion
                                     data-allow-all-closed="true">

                                    <div class="page-profiles__container__accordion__item accordion-item"
                                         data-accordion-item>
                                        <a href="#"
                                           class="page-profiles__container__accordion__item--link accordion-title row columns">
                                            <div
                                                class="page-profiles__container__accordion__item--link--img small-12 large-4 columns np">
                                                <div class="square-box">
                                                    <div class="square-img"><img
                                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/upload/1.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="page-profiles__container__accordion__item--link--txt small-12 large-8 columns name">
                                                <h2>Nazwa zespołu</h2>
                                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper.
                                                    Suspendisse a pellentesque dui, non felis. Maecenas
                                                    malesuada elit lectus felis, malesuada ultricies. Curabitur
                                                    et ligula. Ut molestie a. </p>
                                            </div>
                                        </a>
                                        <div
                                            class="page-profiles__container__accordion__item--content accordion-content row columns"
                                            data-tab-content>

                                            <div
                                                class="page-profiles__container__accordion__item--content--txt small-12 large-8 large-push-4 columns">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                    do eiusmod
                                                    tempor
                                                    incididunt ut labore et dolore magna aliqua. Ut enim ad
                                                    minim veniam, quis
                                                    nostrud
                                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis
                                                    aute
                                                    irure dolor in reprehenderit in voluptate velit esse cillum
                                                    dolore eu fugiat
                                                    nulla
                                                    pariatur. Excepteur sint occaecat cupidatat non proident,
                                                    sunt in culpa qui
                                                    officia
                                                    deserunt mollit anim id est laborum. Lorem ipsum dolor sit
                                                    amet, consectetur
                                                    adipiscing elit, sed do eiusmod
                                                    tempor.</p>
                                                <a href="#" class="more-link">zamów</a>
                                            </div>

                                            <div
                                                class="page-profiles__container__accordion__item--content--list small-12 large-4 small-push-12 large-pull-8 columns">
                                                <ul class="no-bullet">
                                                    <li><a href="#">www.stronainternetowa.pl</a></li>
                                                    <li><a href="#">email@email.pl</a></li>
                                                    <li><a href="#">+48 111 222 333</a></li>
                                                    <li><a href="#">facebook</a></li>
                                                    <li><a href="#">youtube</a></li>
                                                </ul>
                                            </div>

                                            <div
                                                class="page-profiles__container__accordion__item--content--video small-12 small-pull-12 columns">
                                                <iframe width="560" height="315"
                                                        src="https://www.youtube.com/embed/2eeKaTPe_pY"
                                                        frameborder="0" allowfullscreen></iframe>
                                                <a class="close-accordion"></a>
                                            </div>
                                        </div>
                                        <a href="#" class="more-link">zamów</a>
                                    </div>


                                    <div class="page-profiles__container__accordion__item accordion-item"
                                         data-accordion-item>

                                        <a href="#"
                                           class="page-profiles__container__accordion__item--link accordion-title row columns">
                                            <div
                                                class="page-profiles__container__accordion__item--link--img small-12 large-4 columns np">
                                                <div class="square-box">
                                                    <div class="square-img"><img
                                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/upload/13.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="page-profiles__container__accordion__item--link--txt small-12 large-8 columns name">
                                                <h2>Nazwa zespołu</h2>
                                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper.
                                                    Suspendisse a pellentesque dui, non felis. Maecenas
                                                    malesuada elit lectus felis, malesuada ultricies. Curabitur
                                                    et ligula. Ut molestie a. </p>
                                            </div>
                                        </a>
                                        <div
                                            class="page-profiles__container__accordion__item--content accordion-content row columns"
                                            data-tab-content>

                                            <div
                                                class="page-profiles__container__accordion__item--content--txt small-12 large-8 large-push-4 columns">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                    do eiusmod
                                                    tempor
                                                    incididunt ut labore et dolore magna aliqua. Ut enim ad
                                                    minim veniam, quis
                                                    nostrud
                                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis
                                                    aute
                                                    irure dolor in reprehenderit in voluptate velit esse cillum
                                                    dolore eu fugiat
                                                    nulla
                                                    pariatur. Excepteur sint occaecat cupidatat non proident,
                                                    sunt in culpa qui
                                                    officia
                                                    deserunt mollit anim id est laborum. Lorem ipsum dolor sit
                                                    amet, consectetur
                                                    adipiscing elit, sed do eiusmod
                                                    tempor.</p>
                                                <a data-open="zespol1" class="more-link">zamów</a>

                                            </div>

                                            <div
                                                class="page-profiles__container__accordion__item--content--list small-12 large-4 small-push-12 large-pull-8 columns">
                                                <ul class="no-bullet">
                                                    <li><a href="#">www.stronainternetowa.pl</a></li>
                                                    <li><a href="#">email@email.pl</a></li>
                                                    <li><a href="#">+48 111 222 333</a></li>
                                                    <li><a href="#">facebook</a></li>
                                                    <li><a href="#">youtube</a></li>
                                                </ul>
                                            </div>

                                            <div
                                                class="page-profiles__container__accordion__item--content--video small-12 small-pull-12 columns">
                                                <iframe width="560" height="315"
                                                        src="https://www.youtube.com/embed/2eeKaTPe_pY"
                                                        frameborder="0" allowfullscreen></iframe>
                                                <a class="close-accordion"></a>
                                            </div>
                                        </div>
                                        <a data-open="zespol1" class="more-link">zamów</a>

                                        <div class="large reveal" id="zespol1" data-reveal>
                                            <div class="modal row">
                                                <div class="modal__container small-12 large-6 large-centered columns">
                                                    <h2>Wypełnij i zamów</h2>
                                                    <form>
                                                        <label>email</label>
                                                        <input type="text"/>
                                                        <label>przewidywana data imprezy</label>
                                                        <select>
                                                            <option hidden></option>
                                                            <option>kalendarz z contact form 7</option>
                                                        </select>
                                                        <label>wiadomość</label>
                                                        <textarea rows="5"></textarea>
                                                        <input type="submit"/>
                                                    </form>
                                                    <button class="close-button" data-close
                                                            aria-label="Close modal" type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <a class="infinite-more" href="#">więcej</a>
                                </div>


                            </div>

                        </div>


                    </section> <!-- end article section -->

                    <footer class="article-footer">

                    </footer> <!-- end article footer -->

                </article> <!-- end article -->
            </main> <!-- end #main -->

        </div> <!-- end #inner-content -->

    </div> <!-- end #content -->

<?php get_footer(); ?>