<?php
/*
Template Name: Template Newsletter
*/
?>

<?php get_header(); ?>

<div id="content" class="clearfix">

                <div id="inner-content" class="clearfix">

                    <main id="main" class="large-12 medium-12 columns np" role="main">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <?php get_template_part( 'parts/loop', 'page-newsletter' ); ?>

                        <?php endwhile; endif; ?>

                    </main> <!-- end #main -->

                </div> <!-- end #inner-content -->

            </div> <!-- end #content -->

<?php get_footer(); ?>