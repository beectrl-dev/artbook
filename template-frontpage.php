<?php
/*
Template Name: Template FrontPage
*/
?>

<?php get_header(); ?>

    <div class="mainview">
        <div class="mainview__center small-12 large-6 large-centered columns"><p>Profesjonalne narzędzie
                współpracy organizatorów z biznesem artystycznym.</p></div>
        <div class="mainview__bottom small-12 columns">
            <div class="mainview__bottom__left show-for-large">
                        <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4810.7 8046 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M89.8,14.3a8.247,8.247,0,0,1-2.6,1,3.991,3.991,0,0,0-3-1.3,4.034,4.034,0,0,0-4.1,4,2.769,2.769,0,0,0,.1.9,12.234,12.234,0,0,1-8.5-4.2,3.606,3.606,0,0,0-.6,2,4.172,4.172,0,0,0,1.8,3.4,4.136,4.136,0,0,1-1.9-.5v.1a4.074,4.074,0,0,0,3.3,4,3.75,3.75,0,0,1-1.1.1,2.2,2.2,0,0,1-.8-.1,3.992,3.992,0,0,0,3.8,2.8,8.24,8.24,0,0,1-5.1,1.7,3.4,3.4,0,0,1-1-.1,12.194,12.194,0,0,0,6.3,1.8A11.582,11.582,0,0,0,88.1,18.4v-.5a7.957,7.957,0,0,0,2-2.1,8.44,8.44,0,0,1-2.4.6,3.362,3.362,0,0,0,2.1-2.1M79.3,0a22,22,0,1,0,22,22,22,22,0,0,0-22-22m0,42a20,20,0,1,1,20-20,20.059,20.059,0,0,1-20,20"
                                      transform="translate(-4868 8046)"/>
                            </svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4745 8046 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M22,0A22,22,0,1,0,44,22,22,22,0,0,0,22,0m0,42A20,20,0,1,1,42,22,20.059,20.059,0,0,1,22,42m2-24.8c0-.8.1-1.2,1.3-1.2H27V13H24.3c-3.2,0-4.3,1.5-4.3,4v2H18v3h2v9h4V22h2.7l.4-3h-3Z"
                                      transform="translate(-4745 8046)"/>
                            </svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="-4679 8045.9 44 44">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                        }
                                    </style>
                                </defs>
                                <path id="path" class="cls-1"
                                      d="M43.1,94.8h.4a1.666,1.666,0,0,0,1.7-1.6V90.5a1.605,1.605,0,0,0-1.7-1.6h-.4a1.666,1.666,0,0,0-1.7,1.6v2.7a1.735,1.735,0,0,0,1.7,1.6m-.4-4.4a.6.6,0,0,1,1.2,0v2.9a.6.6,0,0,1-1.2,0Zm4.6,4.4a2.93,2.93,0,0,0,1.6-.7v.6H50V88.8H48.8v4.5s-.4.5-.8.5-.4-.3-.4-.3V88.8H46.4V94c-.1,0,0,.8.9.8m-9-.1h1.4V91.5L41.4,87H40l-1,3-1-3H36.6l1.8,4.5v3.2Zm8.1,6.8a.855.855,0,0,0-.6.3v4.1a.855.855,0,0,0,.6.3c.6,0,.6-.7.6-.7v-3.3s-.1-.7-.6-.7M44,76.9a22,22,0,1,0,22,22,22,22,0,0,0-22-22m0,42a20,20,0,1,1,20-20,20.059,20.059,0,0,1-20,20m7.6-22.8s-3.8-.2-7.6-.2-7.6.2-7.6.2a3.031,3.031,0,0,0-3.1,3,24.217,24.217,0,0,0,0,7.6,3.1,3.1,0,0,0,3.1,3s3.7.2,7.6.2c3.7,0,7.6-.2,7.6-.2a3.031,3.031,0,0,0,3.1-3,24.217,24.217,0,0,0,0-7.6,3.1,3.1,0,0,0-3.1-3M39.5,99.5H38v7.8H36.6V99.5H35V98.2h4.5Zm3.9,7.8H42.2v-.6a2.487,2.487,0,0,1-1.5.7.911.911,0,0,1-.9-.8v-6.1H41v5.7s0,.3.4.3.8-.5.8-.5v-5.5h1.2Zm4.9-1.5s0,1.6-1.1,1.6a1.515,1.515,0,0,1-1.3-.7v.6H44.6V98.2h1.3v2.9a2.024,2.024,0,0,1,1.3-.7c.8,0,1.1.7,1.1,1.6Zm4.8-3.7v2H50.5v1.5s0,.7.6.7.6-.7.6-.7v-.7H53V106a1.759,1.759,0,0,1-1.9,1.4,1.92,1.92,0,0,1-2-1.4v-3.8a1.774,1.774,0,0,1,2-1.7c2.1,0,2,1.6,2,1.6m-1.9-.6c-.6,0-.6.7-.6.7v.9h1.2v-.9s0-.7-.6-.7"
                                      transform="translate(-4701 7969)"/>
                            </svg></span>
            </div>
            <div class="mainview__bottom__right">
                <form>
                    <input type="search" placeholder="wyszukaj"/>
                </form>
            </div>
            <div class="mainview__bottom__arrow show-for-large">
                <div class="pulse">
                    <span></span>
                </div>
                <span class="down"></span>
            </div>
        </div>
    </div>
    <div id="opinie" class="opinions row">
        <div class="opinions__slider owl-carousel owl-theme">
            <div class="opinions__slider__item small-10 small-centered columns">
                <div class="opinions__slider__item__opinion"><p>Tu będzie wpisana opinia klientów"</p></div>
                <div class="opinions__slider__item__name"><p>Imię Nazwisko</p>
                    <p>BeeCtrl</p></div>
            </div>
            <div class="opinions__slider__item small-10 small-centered columns">
                <div class="opinions__slider__item__opinion"><p>Tu będzie wpisana opinia klientów"</p></div>
                <div class="opinions__slider__item__name"><p>Imię Nazwisko</p>
                    <p>BeeCtrl</p></div>
            </div>
            <div> Your Content</div>
            <div> Your Content</div>
            <div> Your Content</div>
            <div> Your Content</div>
            <div> Your Content</div>
        </div>
    </div>

    <div id="content" class="clearfix">

        <div id="inner-content" class="clearfix">

            <main id="main" class="large-12 medium-12 columns np" role="main">
                <section id="onas" class="about clearfix">
                    <div class="about__us small-12 columns">
                        <div class="about__us__txt small-12 large-6 large-centered columns">
                            <h1>O nas</h1>
                            <p>Informator Artystyczny stanowi kompleksowe kompendium wiedzy stworzone z myślą o
                                organizatorach wszelkiego rodzaju imprez, eventów. Stanowi narzędzie
                                umożliwiające nawiązanie współpracy pomiędzy organizatorami a wykonawcami.
                                Sprawdź jakie korzyści wynikają ze współpracy z nami.</p>
                        </div>
                    </div>

                    <div class="about__details small-12 columns np" data-equalizer data-equalize-on="large">
                        <div class="about__details__organizers small-12 large-6 columns">
                            <div
                                class="about__details__organizers__txt small-12 large-10 xlarge-8 large-centered columns"
                                data-equalizer-watch>
                                <h2>Organizatorzy</h2>
                                <ul>Jeśli jesteś organizatorem dzięki nam otrzymujesz:
                                    <li>Coroczny bezpłatny egzemplarz katalogu, z kopalnią pomysłów na Twoje imprezy</li>
                                    <li>Narzędzie umożliwiające bezpośredni kontakt z artystami</li>
                                    <li>Nasze pełne bezpłatne wsparcie przy organizacji imprez</li>
                                </ul>
                                <a class="more-link" href="#">zapytaj</a>
                            </div>
                        </div>

                        <div class="about__details__artists small-12 large-6 columns">
                            <div
                                class="about__details__artists__txt small-12 large-10 xlarge-8 large-centered columns"
                                data-equalizer-watch>
                                <h2>Zespoły, artyści, firmy</h2>
                                <ul>Jeśli pracujesz w branży artystycznej dzięki współpracy otrzymujesz:
                                <li>Dystrybucje Twojej oferty do wszystkich organizatorów imprez w Polsce oraz Europie
                                </li>
                                <li>Profil na stronie www z możliwością promocji kanału YouTube</li>
                                <li>Newsletter artystyczny, docierający w formie elektronicznej do ponad 24.000 Twoich
                                    potencjalnych klientów.
                                </li>
                                <li>Wsparcie naszych art-menadżerów w zakresie związanym na managementem.</li>
                                </ul>
                                <a class="more-link" href="#">zapytaj</a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="oferta" class="offer clearfix">
                    <h1>Oferta</h1>
                    <div class="offer__catalog row">
                        <div class="offer__catalog__left small-12 large-4 large-offset-1 columns">
                            <h2>Pokaż swoją ofertę w katalogu</h2>
                            <p>Katalog wydawany jest w nakładzie 7000 egzemplarzy. Otrzymują go wszystkie
                                domy, kultury, urzędy, wydziały, promocji, starostwa powiatowe, samorządy
                                studenckie, agencje artystyczne, eventowe, ambasady, konsulaty. Jako klient
                                otrzymasz dokumenty potwierdzające realizację wysyłki.</p>
                            <a class="more-link" href="#">więcej</a>
                        </div>
                        <div class="offer__catalog__right show-for-large small-12 large-5 columns">
                            <img id="img-catalog" src="<?php echo get_template_directory_uri(); ?>/assets/images/artbook_catalog.png"
                                 alt="Katalog"/>
                        </div>
                    </div>
                    <div class="offer__newsletter">
                        <div class="row">
                            <div class="offer__newsletter__right small-12 large-4 large-offset-7 columns">
                                <h2>Wyślij swój newsletter artystyczny</h2>
                                <p>Newsletter stanowi elektroniczną formę przekazywania oferty do ponad 24.000
                                    potencjalnych klientów. 48 godzin po dostępny jest raport z wysyłki.
                                    Otrzymują
                                    wszyscy organizatorzy budżetowi, firmy, korporacje, agencje artystyczne,
                                    eventowe.</p>
                                <a class="more-link" href="#">więcej</a>
                            </div>
                        </div>
                    </div>

                    <div class="offer__join row">
                        <div class="offer__join__left small-12 large-4 large-offset-1 columns">
                            <h2>Dołącz do naszego impreseriatu</h2>
                            <p>Nasz impresariat tworzą artyści, zespoły, firmy które prezentują swoje oferty w
                                Informatorze. Od stycznia nasi art-menadżerowie będą bezpośrednio pomagać w
                                zawarciu kontraktów z organizatorami.</p>
                            <a class="more-link" href="#">więcej</a>
                        </div>
                        <div class="offer__join__right show-for-large small-12 large-5 columns">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/impresariat_join.png"
                                 alt="dolacz do impreseriatu"/>
                        </div>
                    </div>

                    <div class="offer__download small-12 columns">
                        <div class="offer__download__txt small-12 large-6 large-centered columns">
                            <p>Zobacz katalog 2017</p>
                            <a class="more-link" href="#">PDF</a>
                        </div>
                    </div>
                </section>

                <section id="kategorie" class="categories clearfix">
                    <h2 class="categories__title small-12 columns">Znajdź podmiot, który odpowiada Twoim potrzebom.</h2>
                    <div class="categories__loop">
                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/muzyka.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Muzyka</h2>
                                <a href="#"></a>
                            </div>
                        </div>

                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/programy_artystyczne.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Programy artystyczne</h2>
                                <a href="#"></a>
                            </div>
                        </div>

                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/organizacja_imprez.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Organizacja imprez</h2>
                                <a href="#"></a>
                            </div>
                        </div>

                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/imprezy_firmowe.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Imprezy firmowe</h2>
                                <a href="#"></a>
                            </div>
                        </div>

                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/technika_sceniczna.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Technika sceniczna</h2>
                                <a href="#"></a>
                            </div>
                        </div>

                        <div class="categories__loop__item small-12 large-4 columns np">
                            <div class="categories__loop__item--background"
                                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/lokalizacje.png');"></div>
                            <div class="categories__loop__item--overlay"></div>
                            <div class="categories__loop__item--title"><h2>Lokalizacje</h2>
                                <a href="#"></a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="klienci" class="ourclients row">
                    <h1>Nasi klienci</h1>
                    <div class="ourclients__slider owl-carousel owl-theme">
                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>
                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>

                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>

                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>

                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>

                        <div class="ourclients__slider__item small-10 small-centered columns">
                            <div class="ourclients__slider__item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png">
                            </div>
                        </div>
                    </div>
                </section>

                <div class="newsletter clearfix">
                    <div class="newsletter__txt small-12 large-6 large-centered columns">
                        <p>Zapisz się do naszego newslettera
                            <span>bądź na bieżąco ze wszystkimi nowościami</span></p>
                        <form>
                            <input type="text" placeholder="wpisz swój adres email"/>
                            <button type="submit" class="more-link">Subskrybuj</button>
                        </form>
                    </div>
                </div>

                <section id="impresariat" class="impresariat clearfix">
                    <div class="row">
                        <div class="small-12 large-6 float-right columns">
                            <h2>Impresariat</h2>
                            <p>W naszym impresariacie współpracujemy z większością artystów, zespołów polskiej
                                sceny muzycznej. Ceny za koncerty są uzyskane bezpośrednio od managerów, więc
                                nie ma obawy o doliczoną marżę agencyjną. Kompleksowo przygotowujemy oferty na
                                różnego rodzaju wydarzenia artystyczne, wspierając organizatorów zarówno
                                budżetowych jak i prywatne firmy.</p>
                            <a class="more-link" href="#">zapytaj</a>
                        </div>
                    </div>
                </section>

                <section id="kontakt" class="contact clearfix row">
                    <h1>Kontakt</h1>
                    <div class="contact__container">
                        <div class="contact__container__left small-12 large-6 columns">
                            <div class="contact__container__left__info row">
                                <p class="large-9 columns">Nasz zespół składa się ze specjalistów z różnych
                                    dziedzin, którzy łączą swoje siły we wspólnym celu.</p>
                                <p class="large-12 columns">Lorem ipsum dolor sit amet enim. Etiam ullamcorper.
                                    Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus
                                    felis, malesuada ultricies. Curabitur et ligula. </p>
                            </div>
                            <div class="contact__container__left__print show-for-large">
                                <p>Nasz katalog drukuje firma:</p>
                                <ul class="no-bullet">
                                    <li>Nazwa firmy</li>
                                    <li>ul. Uliczna 1/1</li>
                                    <li>80-800 Miasto</li>
                                    <li>tel.: +48 111 222 333</li>
                                    <li>email: email@email.pl</li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact__container__right small-12 large-6 columns">
                            <div class="contact__container__right__item small-12 large-6 columns">
                                <div class="contact__container__right__item__details">
                                    <img src="http://placehold.it/180x150">
                                    <ul class="no-bullet">
                                        <li>Imię Nazwisko</li>
                                        <li>stanowisko</li>
                                        <li>email@email.com</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="contact__container__right__item small-12 large-6 columns">
                                <div class="contact__container__right__item__details">
                                    <img src="http://placehold.it/180x150">
                                    <ul class="no-bullet">
                                        <li>Imię Nazwisko</li>
                                        <li>stanowisko</li>
                                        <li>email@email.com</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="contact__container__right__item small-12 large-6 columns">
                                <div class="contact__container__right__item__details">
                                    <img src="http://placehold.it/180x150">
                                    <ul class="no-bullet">
                                        <li>Imię Nazwisko</li>
                                        <li>stanowisko</li>
                                        <li>email@email.com</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="contact__container__right__item small-12 large-6 columns">
                                <div class="contact__container__right__item__details">
                                    <img src="http://placehold.it/180x150">
                                    <ul class="no-bullet">
                                        <li>Imię Nazwisko</li>
                                        <li>stanowisko</li>
                                        <li>email@email.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contact__form">
                        <div class="contact__form__left small-12 large-6 columns">
                            <form>
                                <label>email</label>
                                <input type="text">

                                <label>temat</label>
                                <input type="text">

                                <label>wiadomość</label>
                                <textarea rows="5"></textarea>
                                <input type="submit">
                            </form>
                        </div>
                        <div class="contact__form__right small-12 large-6 columns">
                            <p class="small-12 columns">Zapytaj nas o szczegóły oferty. Chętnie odpowiemy na
                                wszystkie Twoje pytania.</p>
                            <p class="small-12 columns">Lorem ipsum dolor sit amet enim. Etiam ullamcorper.
                                Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis,
                                malesuada ultricies. Curabitur et ligula. Lorem ipsum dolor sit amet enim. Etiam
                                ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit
                                amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. </p>
                        </div>
                    </div>
                </section>
            </main> <!-- end #main -->

        </div> <!-- end #inner-content -->

    </div> <!-- end #content -->

<?php get_footer(); ?>