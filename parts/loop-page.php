<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	<div class="photo">
		<div class="photo__back">
			<a class="photo__back--prev" href="#">Wróć</a>
		</div>
	</div>
	<header class="page-subtitle">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
	    <?php the_content(); ?>
	    <?php wp_link_pages(); ?>


		<div class="details">
			<div class="details__txt small-12 large-7 large-centered columns">
				<p>zapytaj o szczegóły oferty</p>
				<a data-open="details" class="more-link">zapytaj</a>

				<div class="large reveal" id="details" data-reveal>
					<div class="modal row">
						<div class="modal__container small-12 large-6 large-centered columns">
							<h2>Wypełnij i zamów</h2>
							<form>
								<label>email</label>
								<input type="text" />
								<label>wiadomość</label>
								<textarea rows="5"></textarea>
								<input type="submit"/>
							</form>
							<button class="close-button" data-close
									aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->