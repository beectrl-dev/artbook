<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
    <div class="photo">
        <div class="photo__back">
            <a class="photo__back--prev" href="#">Wróć</a>
        </div>
        <h1 class="page-title"><?php the_title()?></h1>
    </div>

    <header class="article-header">
        <h2 class="page-subtitle">Nakład</h2>
    </header> <!-- end article header -->


    <section class="entry-content" itemprop="articleBody">
        <div class="page-offer row">
            <div class="small-12 large-8 large-centered columns">
                <?php the_content(); ?>
                <?php wp_link_pages(); ?>
            </div>

            <div class="page-offer__categories small-12 columns">
                <h2 class="page-subtitle">Kategorie</h2>
                <ul class="accordion" data-accordion data-allow-all-closed="true">
                    <li class="accordion-item is-active" data-accordion-item>
                        <a href="#" class="accordion-title">Muzyka</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="row">
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Programy artystyczne</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="row">
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Kompleksowa organizacja imprez,
                            eventów</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="row">
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Technika, zaplecza sceny,
                            urządzenia</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="row">
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Lokalizacje</a>
                        <div class="accordion-content" data-tab-content>
                            <div class="row">
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>

                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                                <div class="small-12 large-3 columns"><a href="#">nazwa
                                        podkategorii</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <!-- ... -->
                </ul>
            </div>


            <div class="page-offer__price small-12 columns">
                <h2 class="page-subtitle">Cennik</h2>
                <div class="row">
                    <div class="page-offer__price__item small-12 large-4 columns">
                        <div class="page-offer__price__item__spin">
                            <div class="page-offer__price__item__spin__cost">
                                <span>1680 zł</span>
                            </div>
                            <div class="page-offer__price__item__spin__img"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/pagefull.png" alt="1 strona"/></div>
                            <div class="page-offer__price__item__spin__details">
                                <div class="page-offer__price__item__spin__details__info">
                                    <span>1 strona</span>
                                    <span>165x240 mm</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__profil">
                                    <span>profil www</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__gratis">
                                    <span>3* newsletter artystyczny GRATIS</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-offer__price__item small-12 large-4 columns">
                        <div class="page-offer__price__item__spin">
                            <div class="page-offer__price__item__spin__cost"><span>890 zł</span>
                            </div>
                            <div class="page-offer__price__item__spin__img"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/pagehalf.png" alt="1/2 strony"/></div>
                            <div class="page-offer__price__item__spin__details">
                                <div class="page-offer__price__item__spin__details__info">
                                    <span>1/2 strony</span>
                                    <span>165x120 mm</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__profil">
                                    <span>profil www</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__gratis">
                                    <span>2* newsletter artystyczny GRATIS</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="page-offer__price__item small-12 large-4 columns">
                        <div class="page-offer__price__item__spin">
                            <div class="page-offer__price__item__spin__cost"><span>590 zł</span>
                            </div>
                            <div class="page-offer__price__item__spin__img"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/pagequarter.png" alt="1/4 strony"/></div>
                            <div class="page-offer__price__item__spin__details">
                                <div class="page-offer__price__item__spin__details__info">
                                    <span>1/4 strony</span>
                                    <span>165x60 mm</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__profil">
                                    <span>profil www</span>
                                </div>

                                <div class="page-offer__price__item__spin__details__gratis">
                                    <span>newsletter artystyczny GRATIS</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="page-offer__info small-12 large-8 large-centered columns">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                    aute
                    irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla
                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                    officia
                    deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod
                    tempor.</p>
            </div>
        </div>

        <div class="details">
            <div class="details__txt small-12 large-7 large-centered columns">
                <p>zapytaj o szczegóły oferty</p>
                <a data-open="details" class="more-link">zapytaj</a>

                <div class="large reveal" id="details" data-reveal>
                    <div class="modal row">
                        <div class="modal__container small-12 large-6 large-centered columns">
                            <h2>Wypełnij i zamów</h2>
                            <form>
                                <label>email</label>
                                <input type="text"/>
                                <label>wiadomość</label>
                                <textarea rows="5"></textarea>
                                <input type="submit"/>
                            </form>
                            <button class="close-button" data-close
                                    aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section> <!-- end article section -->


    <footer class="article-footer">

    </footer> <!-- end article footer -->


</article> <!-- end article -->