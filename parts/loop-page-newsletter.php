<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
    <div class="photo">
        <div class="photo__back">
            <a class="photo__back--prev" href="#">Wróć</a>
        </div>
        <h1 class="page-title"><?php the_title()?></h1>
    </div>

    <header class="article-header">
        <h2 class="page-subtitle">Zalety</h2>
    </header> <!-- end article header -->


    <section class="entry-content" itemprop="articleBody">
        <?php the_content(); ?>
        <?php wp_link_pages(); ?>
    </section> <!-- end article section -->


    <footer class="article-footer">

    </footer> <!-- end article footer -->


</article> <!-- end article -->