<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.js', array( 'jquery' ), '6.2.3', true );

  // Adding owl carousel
  wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/vendor/owl.carousel/dist/owl.carousel.min.js', array(), '', true );

  // Adding  GSap
  wp_enqueue_script( 'gsap', get_template_directory_uri() . '/vendor/gsap/src/minified/TweenMax.min.js', array(), '', true );

  // Adding ScrollMagic
  wp_enqueue_script( 'scrollmagic', get_template_directory_uri() . '/vendor/scrollmagic/scrollmagic/minified/ScrollMagic.min.js', array(), '', true );
  wp_enqueue_script( 'scrollmagic-gsap-animation', get_template_directory_uri() . '/vendor/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js', array(), '', true );
  wp_enqueue_script( 'scrollmagic-gsap-debug', get_template_directory_uri() . '/vendor/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js', array(), '', true );

  // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

      // Register main stylesheet
      wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/vendor/owl.carousel/dist/assets/owl.carousel.min.css', array(), '', 'all' );
      wp_enqueue_style( 'owl-carousel-css-default', get_template_directory_uri() . '/vendor/owl.carousel/dist/assets/owl.theme.default.min.css', array(), '', 'all' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
