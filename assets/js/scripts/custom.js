/**
 * Created by Pawel on 12.10.2016.
 */
jQuery(document).ready(function($){
    $('.opinions__slider').owlCarousel({
        items: 1,
        dots: false,
        nav: true,
        navText: [
            "<i class='arr left'></i>",
            "<i class='arr right'></i>"
        ],
    });

    $('.ourclients__slider').owlCarousel({
        dots: false,
        nav: true,
        responsive:{
            0:{
                items:1
            },
            1024:{
                items:4
            }
        },
        navText: [
            "<i class='arr left'></i>",
            "<i class='arr right'></i>"
        ],
    });

    var controller = new ScrollMagic.Controller();

    var tl_offer_catalog = new TimelineMax();;
    tl_offer_catalog.fromTo(".offer__catalog__right", 1.4,{autoAlpha:0, left: 300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true})
        .fromTo(".offer__catalog__left", 1.4,{autoAlpha:0, left: -300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true},"")

    var scene_offer_catalog = new ScrollMagic.Scene({
        triggerElement: ".offer__catalog__left",
        offset: -150,
        duration: 0
    })
        .setTween(tl_offer_catalog)
     //   .addIndicators()
        .addTo(controller);

    var tl_offer_newsletter = new TimelineMax();
    tl_offer_newsletter.fromTo(".offer__newsletter__right", 1.4,{autoAlpha:0, left: 300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true},"+0.5")
            .fromTo(".offer__newsletter", 1,{autoAlpha:0, left: -150},{autoAlpha:1, left:0, ease: Sine.easeOut, yoyo: true},"")

    var scene_offer_newsletter = new ScrollMagic.Scene({
        triggerElement: ".offer__newsletter__right",
        offset: -200,
        duration: 0
    })
        .setTween(tl_offer_newsletter)
     //   .addIndicators()
        .addTo(controller);


    var tl_offer_join = new TimelineMax();
    tl_offer_join.fromTo(".offer__join__left", 1.4,{autoAlpha:0, left: -300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true},"")
        .fromTo(".offer__join__right img", 1.5,{autoAlpha:0, bottom: -300},{autoAlpha:1, bottom:0, ease: Sine.easeOut, yoyo: true},"")

    var scene_offer_join = new ScrollMagic.Scene({
        triggerElement: ".offer__join",
        offset: -200,
        duration: 0
    })
        .setTween(tl_offer_join)
      //  .addIndicators()
        .addTo(controller);


    var tl_about_us = new TimelineMax();
    tl_about_us.fromTo(".about__us__txt", 1.4,{autoAlpha:0, bottom: -100},{autoAlpha:1, bottom: 0, ease: Sine.easeOut, yoyo: true},"")

    var scene_offer_join = new ScrollMagic.Scene({
        triggerElement: ".about__us",
        offset: -200,
        duration: 0
    })
        .setTween(tl_about_us)
       // .addIndicators()
        .addTo(controller);

    var tl_about_details = new TimelineMax();
    tl_about_details.fromTo(".about__details__organizers__txt", 1.4,{autoAlpha:0, left: -300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true},"")
        .fromTo(".about__details__artists__txt", 1.4,{autoAlpha:0, left: 300},{autoAlpha:1, left: 0, ease: Sine.easeOut, yoyo: true},"")

    var scene_about_details = new ScrollMagic.Scene({
        triggerElement: ".about__details",
        offset: -200,
        duration: 0
    })
        .setTween(tl_about_details)
      //  .addIndicators()
        .addTo(controller);


});